﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TravelingSalesmanProblem
{
    public class PointWithName
    {
        private static int countObject = 0;
        private static string NAME_LETTERS = "ABCDEFGHIJKLMNOPQSTWVUXZ";

        public static int MAX_POINTS_AUTONAME => NAME_LETTERS.Length;

        public string Name { get; private set; }
        public Point Location { get; private set; }

        public double X => Location.X;
        public double Y => Location.Y;

        public double Left => Location.X;
        public double Top => Location.Y;


        public PointWithName(Point p, string name = null)
        {
            if (String.IsNullOrEmpty(name))
                Name = new String(NAME_LETTERS[(countObject % NAME_LETTERS.Length)], 1);
            else
                Name = name;

            Location = p;

            countObject++;
        }

        public override string ToString()
        {
            return String.Format("{0}:   {1,5:0.#} x {2,5:0.#}", Name ?? "(null)", Location.X, Location.Y);
        }

        public override bool Equals( object obj )
        {
            if (obj == null)
                return false;

            if (obj is PointWithName)
                return Equal((PointWithName)obj);

            if (obj is Point)
                return Equal((Point)obj);

            return false;
        }

        public bool Equal(PointWithName p)
        {
            return (Name.Equals(p.Name) && X == p.X && Y == p.Y);
        }


        public bool Equal( Point p )
        {
            return (X == p.X && Y == p.Y);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + X.GetHashCode() + Y.GetHashCode();
        }


        public static implicit operator Point(PointWithName p)
        {
            return p.Location;
        }

        public static void ClearAutoNameIndex()
        {
            countObject = 0;
        }


        public Length LengthToPoint(PointWithName p)
        {
            return new Length(this, p);
        }

    }
}
