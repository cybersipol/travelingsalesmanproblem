﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TravelingSalesmanProblem
{
    public static class VectorExt
    {
        public static Vector PerpendicularClockwise( this Vector vector2 )
        {
            return new Vector(vector2.Y, -vector2.X);
        }

        public static Vector PerpendicularCounterClockwise( this Vector vector2 )
        {
            return new Vector(-vector2.Y, vector2.X);
        }

    }
}
