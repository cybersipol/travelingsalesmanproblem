﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace TravelingSalesmanProblem
{
    public class ShapesGroup
    {
        public UIElementCollection Canvas { get; private set; } = null;
        private List<Shape> Shapes = new List<Shape>();

        public ShapesGroup( UIElementCollection canvas )
        {
            Canvas = canvas ?? throw new ArgumentNullException(nameof(canvas));
        }

        public void AddShape(Shape shape)
        {
            if (shape == null) return;

            RemoveFromCanvas(shape);

            Shapes.Add(shape);
        }


        public void Remove(Shape shape)
        {
            if (shape == null) return;
            if (Shapes.IndexOf(shape) < 0) return;

            RemoveFromCanvas(shape);

            Shapes.Remove(shape);
        }

        public void RemoveAll()
        {
            if (Shapes.Count <= 0) return;

            List<Shape> toRemove = new List<Shape>();
            toRemove.AddRange(Shapes);

            foreach (Shape shape in toRemove)
                Remove(shape);

        }


        public void PaintShapes()
        {
            if (Shapes.Count <= 0) return;

            foreach (Shape shape in Shapes)
                Canvas.Add(shape);

        }


        private void RemoveFromCanvas(Shape shape)
        {
            if (Canvas.IndexOf(shape) >= 0)
                Canvas.Remove(shape);
        }

    }
}
