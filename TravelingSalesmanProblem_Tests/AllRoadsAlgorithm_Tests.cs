﻿using NUnit.Framework;
using TravelingSalesmanProblem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TravelingSalesmanProblem.Unit.Tests
{
    [TestFixture()]
    public class AllRoadsAlgorithm_Tests
    {

        private AllRoadsAlgorithm sut;
        private List<PointWithName> points = null;

        [SetUp]
        public void Init()
        {
            points = null;
            sut = null;
        }



        #region ctor Tests


        [Test()]
        public void ctor_NullPointList_ThrowException()
        {
            Assert.That(() =>
                        {
                            var result = new AllRoadsAlgorithm(null);
                        },
                        Throws.ArgumentNullException

            );
        }


        [Test()]
        public void ctor()
        {
            var result = new AllRoadsAlgorithm(new List<PointWithName>());

            Assert.That(result,
                        Is.Not.Null &
                        Is.InstanceOf<AllRoadsAlgorithm>()
            );
        }


        #endregion



        #region IterateThroughAllRoads Tests


        [Test()]
        public void IterateThroughAllRoads_Scenario_Expected()
        {
            // arrange
            CreateDefaultPoints();
            sut = CreateSUT();

            decimal count = 0;
            sut.OnRoadSelect += ( road ) =>
            {
                Console.WriteLine(road.ToString());
                count++;
            };

            // act
            sut.IterateThroughAllRoads();


            // assert
            Assert.That(count,
                        Is.EqualTo(Factorial.Calculate(points.Count))
                        );
        }


        #endregion

        #region Help funcs

        private AllRoadsAlgorithm CreateSUT( List<PointWithName> listOfPoints)
        {
            sut = new AllRoadsAlgorithm(listOfPoints);
            return sut;
        }

        private AllRoadsAlgorithm CreateSUT() => CreateSUT(points);


        private List<PointWithName> CreateEmptyPoints()
        {
            points = new List<PointWithName>();
            return points;
        }


        private List<PointWithName> CreateDefaultPoints()
        {
            points = CreateEmptyPoints();

            points.Add(new PointWithName(new Point(0, 0), "A"));
            points.Add(new PointWithName(new Point(0, 1), "B"));
            points.Add(new PointWithName(new Point(2, 2), "C"));
            points.Add(new PointWithName(new Point(6, 6), "D"));
            points.Add(new PointWithName(new Point(3, 4), "E"));

            return points;
        }

        #endregion

    }
}