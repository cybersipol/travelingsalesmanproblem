﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelingSalesmanProblem
{
    public static class Factorial
    {
        public static decimal Calculate( decimal n )
        {
            if (n <= 0) return 0;
            if (n == 1) return 1;

            return n * Calculate(n - 1);
        }

    }
}
