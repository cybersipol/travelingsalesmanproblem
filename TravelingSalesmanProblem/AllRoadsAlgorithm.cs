﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelingSalesmanProblem
{
    public class AllRoadsAlgorithm
    {
        public List<PointWithName> Points { get; private set; }
        private decimal PermutIndex { get; set; } = 0;

        public event Action<Road> OnRoadSelect;

        public AllRoadsAlgorithm( List<PointWithName> points )
        {
            Points = points ?? throw new ArgumentNullException(nameof(points));
        }

        
        public void IterateThroughAllRoads() =>
            GetPer(Points.ToArray());

        private static IEnumerable<IEnumerable<T>> GetPermutations<T>( IEnumerable<T> list, int length )
        {
            if (length == 1) return list.Select(t => new T[] { t });

            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    ( t1, t2 ) => t1.Concat(new T[] { t2 }));
        }



        private void Swap( ref PointWithName a, ref PointWithName b )
        {
            if (a.Equals(b)) return;

            PointWithName pom = a;
            a = b;
            b = pom;
        }

        public void GetPer( PointWithName[] list )
        {
            PermutIndex = 0;
            int x = list.Length - 1;
            GetPer(list, 0, x);
        }

        private void GetPer( PointWithName[] list, int k, int m )
        {
            if (k == m)
            {
                Road road = new Road();
                road.Length = 0.0d;

                PointWithName prevPoint = null;
                PointWithName firstPoint = null;
                
                foreach (PointWithName p in list)
                {
                    if (firstPoint == null)
                        firstPoint = p;
                    if (prevPoint != null)
                    {
                        Length l = new Length(p, prevPoint);
                        road.Length += l.Value;
                    }

                    prevPoint = p;
                }

                List<PointWithName> lst = new List<PointWithName>();
                lst.AddRange(list);

                if (prevPoint != null && firstPoint != null)
                {
                    Length l = new Length(firstPoint, prevPoint);
                    road.Length += l.Value;
                    lst.Add(firstPoint);
                }

                road.Points = lst;
                road.Index = PermutIndex++;

                OnRoadSelect?.Invoke(road);
            }
            else
                for (int i = k; i <= m; i++)
                {
                    Swap(ref list[k], ref list[i]);
                    GetPer(list, k + 1, m);
                    Swap(ref list[k], ref list[i]);
                }
        }

    }
}
