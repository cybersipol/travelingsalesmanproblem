﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TravelingSalesmanProblem
{
    public class Length
    {
        public PointWithName Point1 { get; private set; } = null;
        public PointWithName Point2 { get; private set; } = null;

        public string Name { get; private set; } = null;


        public Length( PointWithName point1, PointWithName point2, string name = null)
        {
            Point1 = point1 ?? throw new ArgumentNullException(nameof(point1));
            Point2 = point2 ?? throw new ArgumentNullException(nameof(point2));

            if (String.IsNullOrEmpty(name))
                Name = (Point1.Name ?? "_") + (Point2.Name ?? "_");
            else
                Name = name;

        }

        public double Value => Math.Abs(Point.Subtract(Point1.Location, Point2.Location).Length);


        public override string ToString()
        {
            return String.Format("{0}:  {1,5:0.0}", this.Name ?? "*",  Value);
        }
    }
}
