﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelingSalesmanProblem
{
    public class RoadConst
    {
        public uint LENGTH_OF_LENGTH = 5;
        public uint LENGTH_DECIMAL_POINT = 5;
        public uint LENGTH_INDEX = 5;
        public decimal INDEX_DELTA = 1;
    }
}
