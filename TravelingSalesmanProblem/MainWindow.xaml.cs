﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Linq;

namespace TravelingSalesmanProblem
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            PointWithName.ClearAutoNameIndex();
            sliderRandom.Maximum = PointWithName.MAX_POINTS_AUTONAME;
            this.WindowState = WindowState.Maximized;

        }

        private List<PointWithName> points = new List<PointWithName>();
        private List<Length> AllLengths = new List<Length>();
        private Dictionary<PointWithName, LengthsAndShapes> visibleLines = new Dictionary<PointWithName, LengthsAndShapes>();

        private void Canvas_MouseDown( object sender, MouseButtonEventArgs e )
        {
            Point p = e.GetPosition(pntCanvas);

            AddNewPoint(p);
        }

        private void AddNewPoint( Point p )
        {
            if (points.Count >= PointWithName.MAX_POINTS_AUTONAME) return;

            PointWithName pnt = new PointWithName(p);

            Ellipse el = new Ellipse()
            {
                Fill = new SolidColorBrush(Colors.LightBlue),
                Width = 10,
                Height = 10,
                StrokeThickness = 0
            };

            pntCanvas.Children.Add(el);

            Canvas.SetLeft(el, pnt.X - (el.Width / 2));
            Canvas.SetTop(el, pnt.Y - (el.Height / 2));
            Canvas.SetZIndex(el, 100);

            TextBlock textBlock = new TextBlock();
            textBlock.Text = pnt.Name;
            textBlock.FontSize = 12;
            textBlock.Foreground = new SolidColorBrush(Colors.White);


            pntCanvas.Children.Add(textBlock);

            Canvas.SetLeft(textBlock, pnt.X - (el.Width / 2));
            Canvas.SetTop(textBlock, pnt.Y - el.Height - 11);
            Canvas.SetZIndex(textBlock, 101);


            TreeViewItem itemPoint = new TreeViewItem();
            itemPoint.Header = pnt.ToString();
            itemPoint.Tag = pnt;

            tvItemPoints.Items.Add(itemPoint);
            tvItemPoints.Header = Regex.Replace(tvItemPoints.Header.ToString(), @"\(([0-9]+)\)$", "").Trim() + String.Format(" ({0})", tvItemPoints.Items.Count);

            points.Add(pnt);

            CreateAllLengthInTreeView();
        }

        public List<Length> CreateLengthsFromPoint(PointWithName point)
        {
            if (points.Count <= 1)
                return new List<Length>();

            List<Length> result = new List<Length>();

            foreach (PointWithName p in points)
                if (!p.Equal(point))
                    result.Add(new Length(point, p));

            return (from r in result orderby r.Value select r).ToList();
        }

        public List<Road> smallestRoads = new List<Road>();
        public List<Road> biggestRoads = new List<Road>();

        public void CreateAllLengthInTreeView()
        {
            AllLengths.Clear();
            tvItemLengths.Items.Clear();
            tvItemLengths.Header = "Lengths";
            tvItemN.Header = String.Format("Combinantions: {0,5:0,0.#}", Factorial.Calculate(points.Count));
            wayLen.Text = "0";
            if (points.Count <= 1) return;

            int countAll = 0;

            foreach (PointWithName p in points)
            {
                List<Length> lengths = CreateLengthsFromPoint(p);
                AllLengths.AddRange(lengths);
                TreeViewItem pItemLengths = new TreeViewItem();
                pItemLengths.Header = String.Format("{0}  ({1})", p.Name, lengths.Count);
                pItemLengths.IsExpanded = true;
                pItemLengths.Tag = p;

                foreach (Length l in lengths)
                {
                    TreeViewItem itemLen = new TreeViewItem();
                    itemLen.Header = l.ToString();
                    itemLen.Tag = l;
                    pItemLengths.Items.Add(itemLen);
                }


                TreeViewItem empty = new TreeViewItem();
                empty.Header = "";
                empty.IsExpanded = false;

                tvItemLengths.Items.Add(empty);
                tvItemLengths.Items.Add(pItemLengths);

                countAll += lengths.Count;
            }


            tvItemLengths.Header = String.Format("Lengths   {0}", countAll);
            CalculateWayLength();
        }

        private void CalculateWayLength()
        {
            if (visibleLines == null) return;
            if (visibleLines.Count <= 0) return;

            double wayLength = 0d;
            foreach (PointWithName point in visibleLines.Keys)
            {
                LengthsAndShapes lengthsAndShapes = visibleLines[point];
                if (lengthsAndShapes == null) continue;
                if (lengthsAndShapes.length == null) continue;

                wayLength += lengthsAndShapes.length.Value;
            }

            wayLen.Text = String.Format("{0,5:F2}", wayLength);
        }

        private void TvInfo_SelectedItemChanged( object sender, RoutedPropertyChangedEventArgs<object> e )
        {
            if (tvInfo.SelectedItem == null) return;
            if (!(tvInfo.SelectedItem is TreeViewItem)) return;

            TreeViewItem item = tvInfo.SelectedItem as TreeViewItem;

            object tag = item.Tag;
            if (tag is Length)
            {

                Length l = tag as Length;

                PaintLength(l);

                CalculateWayLength();

            }
        }


        private void PaintLength(PointWithName p1, PointWithName p2)
        {
            PaintLength( new Length(p1, p2) );
        }

        private void PaintLength(Length l)
        {
            if (visibleLines.ContainsKey(l.Point1) && visibleLines[l.Point1] != null)
            {
                LengthsAndShapes remGroup = visibleLines[l.Point1];
                remGroup.RemoveAll();
                visibleLines[l.Point1] = null;
            }


            if (visibleLines.ContainsKey(l.Point2) && visibleLines[l.Point2] != null)
            {
                if (visibleLines[l.Point2].length != null && visibleLines[l.Point2].length.Point2.Equal(l.Point1))
                {
                    LengthsAndShapes remGroup = visibleLines[l.Point2];
                    remGroup.RemoveAll();
                    visibleLines[l.Point2] = null;
                }
            }


            ShapesGroup group = new ShapesGroup(pntCanvas.Children);

            List<Shape> shapes = new List<Shape>();

            Line ln = new Line();
            ln.Stroke = Brushes.Gray;

            ln.X1 = l.Point1.X;
            ln.X2 = l.Point2.X;
            ln.Y1 = l.Point1.Y;
            ln.Y2 = l.Point2.Y;

            ln.StrokeThickness = 2;
            Canvas.SetZIndex(ln, 1);

            Vector vector = new Vector(l.Point2.X - l.Point1.X, l.Point2.Y - l.Point1.Y);
            Vector v2 = Vector.Divide(vector, 2d);
            vector.Normalize();

            Vector perPemd1 = vector.PerpendicularClockwise();
            Vector perPemd2 = vector.PerpendicularCounterClockwise();

            Point pArrow = Point.Add(Point.Subtract(l.Point2.Location, v2), vector * 10);
            Point p = Point.Subtract(pArrow, Vector.Multiply(20, vector));

            Point p1 = Point.Add(p, perPemd1 * 5);
            Point p2 = Point.Add(p, perPemd2 * 5);

            // Arrow
            Line[] lines = new Line[] {
                    new Line()
                    {
                        Stroke = Brushes.White,
                        X1 = pArrow.X,
                        Y1 = pArrow.Y,
                        X2 = p1.X,
                        Y2 = p1.Y,
                        StrokeThickness = 2
                    },

                    new Line()
                    {
                        Stroke = Brushes.White,
                        X1 = p1.X,
                        Y1 = p1.Y,
                        X2 = p2.X,
                        Y2 = p2.Y,
                        StrokeThickness = 2
                    },

                    new Line()
                    {
                        Stroke = Brushes.White,
                        X1 = p2.X,
                        Y1 = p2.Y,
                        X2 = pArrow.X,
                        Y2 = pArrow.Y,
                        StrokeThickness = 2,
                    },

                };

            group.AddShape(ln);
            foreach (Line ll in lines)
            {
                Canvas.SetZIndex(ll, 0);
                group.AddShape(ll);
            }

            group.PaintShapes();

            visibleLines[l.Point1] = new LengthsAndShapes(group, l);

        }


        private class LengthsAndShapes
        {
            public LengthsAndShapes()
            {
            }

            public LengthsAndShapes( ShapesGroup shapes, Length length )
            {
                shapesGroup = shapes ?? throw new ArgumentNullException(nameof(shapes));
                this.length = length ?? throw new ArgumentNullException(nameof(length));
            }

            public ShapesGroup shapesGroup { get; private set; } = null;
            public Length length { get; private set; } = null;

            public void RemoveAll()
            {
                if (shapesGroup!=null)
                    shapesGroup.RemoveAll();

                shapesGroup = null;
                length = null;
            }
        }

        
        private void BtnClear_Click( object sender, RoutedEventArgs e )
        {
            pntCanvas.Children.Clear();
            visibleLines.Clear();
            AllLengths.Clear();
            points.Clear();
            tvItemLengths.Items.Clear();
            tvItemPoints.Items.Clear();

            tvItemPoints.Header = "Points";
            tvItemLengths.Header = "Lengths";
            tvItemN.Header = "Combinantions:";
            wayLen.Text = "0";
            PointWithName.ClearAutoNameIndex();
            lvRoadsSmallest.Items.Clear();
            lvRoadsBiggest.Items.Clear();

            GC.Collect();

        }


        private void BtnRandom_Click( object sender, RoutedEventArgs e )
        {
            BtnClear_Click(btnClear, e);

            int countPoint = (int)sliderRandom.Value;

            double maxWidth = pntCanvas.ActualWidth;
            double maxHeight = pntCanvas.ActualHeight;

            Random rnd = new Random();
            for (int i=0;i<countPoint;i++)
            {
                double x = rnd.Next((int)maxWidth);
                double y = rnd.Next((int)maxHeight);

                AddNewPoint(new Point(x, y));
            }
        }

        private void BtnAllRoad_Click( object sender, RoutedEventArgs e )
        {
            lvRoadsSmallest.Items.Clear();
            lvRoadsBiggest.Items.Clear();

            AllRoadsAlgorithm allRoadsAlgorithm = new AllRoadsAlgorithm(points);

            
            RoadConst roadConst = new RoadConst();
            roadConst.LENGTH_DECIMAL_POINT = 2; 

            allRoadsAlgorithm.OnRoadSelect += ( road ) =>
             {
                 road.Const = roadConst;
                 smallestRoads.Add(road);
                 biggestRoads.Add(road);

                 smallestRoads.Sort(( x, y ) => { return (x.Length == y.Length ? (x.Index>y.Index ? 1:-1):(x.Length > y.Length ? 1:-1)); });
                 biggestRoads.Sort(( x, y ) => { return (x.Length == y.Length ? (x.Index > y.Index ? 1 : -1) : (x.Length > y.Length ? -1 : 1)); });

                 if (smallestRoads.Count > 100)
                     smallestRoads.RemoveRange(100, smallestRoads.Count-100);

                 if (biggestRoads.Count > 100)
                     biggestRoads.RemoveRange(100, biggestRoads.Count - 100);
             };

            smallestRoads.Clear();
            biggestRoads.Clear();
            allRoadsAlgorithm.IterateThroughAllRoads();

            foreach (Road road in smallestRoads)
                lvRoadsSmallest.Items.Add(road);

            foreach (Road road in biggestRoads)
                lvRoadsBiggest.Items.Add(road);

        }

        private void Window_SizeChanged( object sender, SizeChangedEventArgs e )
        {
            
        }

        private void LvRoadsSmallest_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if (lvRoadsSmallest.SelectedItem == null)
                return;

            lvRoadsBiggest.SelectedItem = null;

            if (!(lvRoadsSmallest.SelectedItem is Road))
                return;



            Road road = lvRoadsSmallest.SelectedItem as Road;
            
            PointWithName prevPoint = null;
            foreach (PointWithName p in road.Points)
            {
                if (prevPoint!=null)
                    PaintLength(prevPoint, p);

                prevPoint = p;
            }

            CalculateWayLength();

        }

        private void LvRoadsBiggest_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if (lvRoadsBiggest.SelectedItem == null)
                return;

            lvRoadsSmallest.SelectedItem = null;

            if (!(lvRoadsBiggest.SelectedItem is Road))
                return;

            Road road = lvRoadsBiggest.SelectedItem as Road;

            PointWithName prevPoint = null;
            foreach (PointWithName p in road.Points)
            {
                if (prevPoint != null)
                    PaintLength(prevPoint, p);

                prevPoint = p;
            }

            CalculateWayLength();

        }
    }
}
