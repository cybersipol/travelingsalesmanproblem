﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelingSalesmanProblem
{
    public class Road
    {
        public static readonly RoadConst DefaultConst = new RoadConst();
        public RoadConst Const { get; set; } = DefaultConst;

        public List<PointWithName> Points { get; set; } = null;
        public double Length { get; set; } = double.MaxValue;
        public decimal Index { get; set; } = 0;

        public override string ToString()
        {
            if (Points == null) return "(road empty)";
            if (Points.Count<=1) return "(road empty)";

            StringBuilder sb = new StringBuilder(String.Empty);
            sb.Append(String.Format("{0,"+Const.LENGTH_INDEX+"}. ", Index + Const.INDEX_DELTA));
            int j = 0;
            foreach (PointWithName p in Points)
            {
                sb.Append(j > 0 ? " -> " : String.Empty).Append(p.Name);

                j++;
            }

            sb.Append(String.Format(":  {0,"+Const.LENGTH_OF_LENGTH+":F"+Const.LENGTH_DECIMAL_POINT+"}", Length));
            return sb.ToString();
        }
    }
}
